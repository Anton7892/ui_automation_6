package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.Waiter;

public class _15_TGFileUpload extends Base{

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend");
        driver.findElement(By.id("card-13")).click();

    }
    @Test
    public void validateFileUpload(){
        WebElement fileUpload = driver.findElement(By.id("file_upload"));
        WebElement uploadButton = driver.findElement(By.id("file_submit"));


        String fileToUpload = "/Users/anton/IdeaProjects/ui_automation_6/hello.txt";

        fileUpload.sendKeys(fileToUpload);
        uploadButton.click();

        Waiter.pause(3);


    }
        /*
        TEST CASE
         Note create .txt file called "hello.txt" in the root of your project and use it as a file path.
        Go to https://techglobal-training.com/frontend/
        Click on the "File Upload" card
        Send the path of the file as keys to the "Choose File" input box
        Click on the "UPLOAD" button
        Validate the result message equals "You Uploaded 'hello.txt'"
        */
    @Test
    public void fileUploadTest(){
        WebElement fileUpload = driver.findElement(By.id("file_upload"));
        WebElement uploadButton = driver.findElement(By.id("file_submit"));

        String filePath = "/Users/anton/IdeaProjects/ui_automation_6/hello.txt";

        fileUpload.sendKeys(filePath);
        uploadButton.click();


        WebElement result = driver.findElement(By.id("result"));
        Assert.assertEquals(result.getText(),"You Uploaded '" + filePath.substring(filePath.lastIndexOf('/')+1) +"'");
        // were taking the last index from backslash of file, so it is hello.txt




    }
}
