package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.Waiter;

import java.time.Duration;
import java.util.List;

public class _19_TGActionsTest extends Base {
    @BeforeMethod
    public void setPage() {
        driver.get("https://techglobal-training.com/frontend/");
        driver.findElement(By.id("card-15")).click();
        actions = new Actions(driver);

    }

    @Test
    public void mouseActions() {
        Actions actions = new Actions(driver);

        actions.dragAndDrop(driver.findElement(By.id("")), driver.findElement(By.id(""))).perform();
    }

    /*
  Go to https://techglobal-training.com/frontend/
Click on the "Actions" card
Verify that the user is redirected to the Actions page
Verify that the first three web elements are present and labeled as "Click on me", "Right-Click on me", and "Double-Click on me"
Perform a click action on the first web element labeled "Click on me"
Verify that a message appears next to the element stating, "You clicked on a button!"
Perform a right-click action on the second web element labeled "Right-Click on me"
Verify that the message appears next to the element stating, "You right-clicked on a button!"
Perform a double-click action on the third web element labeled "Double-Click on me"
Verify that the message appears next to the element stating, "You double-clicked on a button!"
     */
    @Test
    public void mouseActions2() {

        Assert.assertEquals(driver.getCurrentUrl(), "https://techglobal-training.com/frontend/actions");

        List<WebElement> buttons = driver.findElements(By.className("Button_c_button__w4+7K null"));

        buttons.forEach(bl -> {
            Assert.assertTrue(bl.isDisplayed());
            Assert.assertEquals(bl.getText(), "Click on me");
            Assert.assertEquals(bl.getText(), "Right-Click on me");
            Assert.assertEquals(bl.getText(), "Double-Click on me");
        });

        WebElement clickButton = driver.findElement(By.id("click"));
        actions.moveToElement(clickButton).click().perform();

        WebElement result = driver.findElement(By.id("click_result"));
        Assert.assertEquals(result.getText(), "You clicked on a button!");

        WebElement RightClickButton = driver.findElement(By.id("right-click"));
        actions.moveToElement(RightClickButton).click().perform();

        WebElement result2 = driver.findElement(By.id("right_click_result"));
        Assert.assertEquals(result2.getText(), "You right-clicked on a button!");

        WebElement DoubleClickButton = driver.findElement(By.id("double-click"));
        actions.moveToElement(DoubleClickButton).click().perform();

        WebElement result3 = driver.findElement(By.id("double_click_result"));
        Assert.assertEquals(result3.getText(), "You double-clicked on a button!");

    }

    @Test
    public void dragAndDrop() {
        WebElement dragElementButton = driver.findElement(By.id("drag_element"));
        WebElement dropElementButton = driver.findElement(By.id("drop_element"));


        // actions.dragAndDrop(dragElementButton,dropElementButton).perform();

        actions.moveToElement(dragElementButton).clickAndHold().moveToElement(dropElementButton).release().perform();

        WebElement dragAndDropResult = driver.findElement(By.id("drag_and_drop_result"));

        Waiter.waitForVisibilityOfElement(dragAndDropResult, 30);

        Assert.assertEquals(dragAndDropResult.getText(), "An element dropped here!");

        Waiter.pause(3);

        actions.keyDown(Keys.SHIFT)
                .sendKeys(dropElementButton, "techGlobal")
                .pause(Duration.ofSeconds(2))
                .keyUp(Keys.SHIFT)
                .sendKeys(dragAndDropResult, "techGlobal");
    }

    @Test
    public void keyBoardActions() {
         /*
          Go to https://techglobal-training.com/frontend/
          Click on the "Actions" card
          Go to the input box, and remove if there is an existing text inside
          First, enter “h” to the input box in upper case using keyboard actions
          Then complete the word by sending “ello” as a key
          Validate value attribute of the input box is “Hello”
          */
        //WebElement removeText = driver.findElement(By.id("input_box"));
        //actions.keyDown(Keys.BACK_SPACE).pause(Duration.ofSeconds(2));

        WebElement inputBox = driver.findElement(By.id("input_box"));

        actions.keyDown(Keys.SHIFT)
                .sendKeys(inputBox, "H")
                .keyUp(Keys.SHIFT)
                .pause(Duration.ofSeconds(2))
                .sendKeys("ello")
                .pause(Duration.ofSeconds(2)).perform();

        Assert.assertEquals(inputBox.getAttribute("value"), "Hello");


    }

    @Test
    public void keyBoardActionsTest2() {
        /*
        TEST CASE 4
        Go to https://techglobal-training.com/frontend/
        Click on the "Actions" card
        Go to the input box, and remove if there is an existing text inside
        Enter “techglobal” to input the box with uppercases
        Then, copy the text and paste it again
        Validate the value attribute for the search input box is “TECHGLOBALTECHGLOBAL” (edited)
         */

        WebElement inputBox2 = driver.findElement(By.id("input_box"));
        actions.keyDown(Keys.SHIFT)
                .sendKeys(inputBox2, "techglobal")
                .keyUp(Keys.SHIFT)
                .keyDown(Keys.COMMAND)
                .sendKeys("a")
                .keyDown(Keys.COMMAND)
                .sendKeys("c")
                .sendKeys(Keys.ARROW_RIGHT)
                .keyDown(Keys.COMMAND)
                .sendKeys("v")
              //  .sendKeys("v")
                .pause(Duration.ofSeconds(2)).perform();


    }
}


