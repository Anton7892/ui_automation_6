package java_projects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import scripts.Base;
import utils.Waiter;

import java.util.List;

public class FrontEndProject01 extends Base {

    @BeforeMethod
    public void setPage(){
        driver.get("https://techglobal-training.com/frontend/project-1");

    }

    @Test(priority = 1)
    public void validateContactInformation(){
        WebElement header = driver.findElement(By.className("is-size-2"));
        Assert.assertTrue(header.isDisplayed());
        Assert.assertEquals(header.getText(),"Contact Us");

        WebElement address = driver.findElement(By.id("address"));
        Assert.assertTrue(address.isDisplayed());
        Assert.assertEquals(address.getText(),"2860 S River Rd Suite 350, Des Plaines IL 60018");

        WebElement email = driver.findElement(By.id("email"));
        Assert.assertTrue(email.isDisplayed());
        Assert.assertEquals(email.getText(),"info@techglobalschool.com");

        WebElement phoneNumber = driver.findElement(By.id("phone-number"));
        Assert.assertTrue(phoneNumber.isDisplayed());
        Assert.assertEquals(phoneNumber.getText(),"(773) 257-3010");

    }
    @Test(priority = 2)
    public void validateFullNameInputBox(){
        WebElement inputBox = driver.findElement(By.cssSelector("div:nth-child(1)>div>input"));
        WebElement label = driver.findElement(By.cssSelector("div:nth-child(1) > label"));

        Assert.assertTrue(inputBox.isDisplayed());
        Assert.assertEquals(inputBox.getAttribute("required"),"true");
        Assert.assertEquals(label.getText(), "Full name *");
        Assert.assertEquals(inputBox.getAttribute("placeholder"), "Enter your full name");// Normally it is a bug


    }

    @Test(priority = 3)
    public void validateGenderButton(){
        WebElement genderLabel = driver.findElement(By.xpath("(//label[@class='label'])[2]"));
        List<WebElement> radioButtonsLabel = driver.findElements(By.cssSelector("label[class*='radio']"));
        List<WebElement> radioButtonsInput = driver.findElements(By.cssSelector("input[class*='mr-1']"));

        Assert.assertEquals(genderLabel.getText(), "Gender *"); // Normally it is a bug
        Assert.assertEquals(genderLabel.getAttribute("required"), null);  // not sure this is correct


        String[] expected = {"Male", "Female", "Prefer not to disclose"};

        for (int i = 0; i < expected.length; i++) {
            Assert.assertEquals(radioButtonsLabel.get(i).getText(), expected[i]);
            Assert.assertTrue(radioButtonsLabel.get(i).isEnabled());
            Assert.assertFalse(radioButtonsInput.get(i).isSelected());
        }

        radioButtonsLabel.get(0).click();
        Assert.assertTrue(radioButtonsInput.get(0).isSelected());

        for (int i = 1; i < radioButtonsLabel.size(); i++) {
            Assert.assertFalse(radioButtonsInput.get(i).isSelected());
        }
        radioButtonsLabel.get(1).click();
        Assert.assertTrue(radioButtonsInput.get(1).isSelected());
        for (int i = 0; i < radioButtonsLabel.size(); i++) {
            Assert.assertFalse(radioButtonsInput.get(i).isSelected());
            i++;
        }

    }



    @Test(priority = 4)
    public void validateAddressInputBox() {

        WebElement addressInputBox = driver.findElement(By.cssSelector("div:nth-child(3)>div > input"));
        WebElement addressLabel = driver.findElement(By.xpath("(//label[@class='label'])[3]"));
        Assert.assertTrue(addressInputBox.isDisplayed());
        Assert.assertEquals(addressInputBox.getAttribute("required"), null);
        Assert.assertEquals(addressLabel.getText(), "Address");
        Assert.assertEquals(addressInputBox.getAttribute("placeholder"), "Enter your address");
    }



    @Test(priority = 5)
    public void validateEmailInputBox() {
        WebElement emailInputBox = driver.findElement(By.cssSelector("div:nth-child(4)>div>input"));
        WebElement emailLabel = driver.findElement(By.xpath("(//label[@class='label'])[4]"));
        Assert.assertTrue(emailInputBox.isDisplayed());
        Assert.assertEquals(emailInputBox.getAttribute("required"), "true");
        Assert.assertEquals(emailLabel.getText(), "Email *");
        Assert.assertEquals(emailInputBox.getAttribute("placeholder"), "Enter your email");
    }


    @Test(priority = 6)
    public void validatePhoneInputBox() {
        WebElement phoneInputBox = driver.findElement(By.cssSelector("div:nth-child(5)>div>input"));
        WebElement phoneLabel = driver.findElement(By.xpath("(//label[@class='label'])[5]"));

        Assert.assertTrue(phoneInputBox.isDisplayed());
        Assert.assertEquals(phoneInputBox.getAttribute("required"), null);
        Assert.assertEquals(phoneLabel.getText(), "Phone");
        Assert.assertEquals(phoneInputBox.getAttribute("placeholder"), "Enter your phone number");
    }



    @Test(priority = 7)
    public void validateValidateMessageTextArea() {
        WebElement messageInputBox = driver.findElement(By.cssSelector("div:nth-child(6)>div>textarea"));
        WebElement messageLabel = driver.findElement(By.xpath("(//label[@class='label'])[6]"));
        Assert.assertTrue(messageInputBox.isDisplayed());
        Assert.assertEquals(messageInputBox.getAttribute("required"), null);
        Assert.assertEquals(messageLabel.getText(), "Message");
        Assert.assertEquals(messageInputBox.getAttribute("placeholder"), "Type your message here...");
    }


    @Test(priority = 8)
    public void ValidateTheConsentCheckbox() {

        WebElement consentInput = driver.findElement(By.cssSelector("div:nth-child(7)>div>label>input"));
        WebElement consentLabel = driver.findElement(By.cssSelector("div:nth-child(7)>div>label"));

        Assert.assertTrue(consentLabel.isDisplayed());
        Assert.assertEquals(consentLabel.getText(), "I give my consent to be contacted.");
        Assert.assertEquals(consentInput.getAttribute("required"), "true");

        Assert.assertTrue(consentInput.isEnabled());  // label or input?
        consentInput.click();
        Assert.assertTrue(consentInput.isSelected());
        Waiter.pause(3);
        consentInput.click();
        Assert.assertFalse(consentInput.isSelected());
        Waiter.pause(3);
    }

    @Test(priority = 9)
    public void validateSubmitButton() {
        WebElement submitButton = driver.findElement(By.cssSelector(".is-link"));
        Assert.assertTrue(submitButton.isDisplayed());
        Assert.assertTrue(submitButton.isEnabled());
        Assert.assertEquals(submitButton.getText(), "SUBMIT");
    }


    @Test(priority = 10)
    public void  ValidateTheFormSubmission(){

        WebElement enterFullName = driver.findElement(By.cssSelector("div:nth-child(1)>div>input"));
        List<WebElement> genderOptions = driver.findElements(By.className("radio"));
        WebElement addressInputBox = driver.findElement(By.cssSelector("div:nth-child(3)>div > input"));
        WebElement emailInputBox = driver.findElement(By.cssSelector("div:nth-child(4)>div>input"));
        WebElement phoneInputBox = driver.findElement(By.cssSelector("div:nth-child(5)>div>input"));
        WebElement messageInputBox = driver.findElement(By.cssSelector("div:nth-child(6)>div>textarea"));
        WebElement consentInput = driver.findElement(By.cssSelector("div:nth-child(7)>div>label>input"));
        WebElement submitButton = driver.findElement(By.cssSelector(".is-link"));

        genderOptions.get(0).click();
        enterFullName.sendKeys("John Doe");
        addressInputBox.sendKeys("2860 S River Rd Suite 350, Des Plaines IL 60018");
        emailInputBox.sendKeys("info@techglobalschool.com");
        phoneInputBox.sendKeys("(773) 257-3010");
        messageInputBox.sendKeys("Automation test is fun!");
        consentInput.click();
        Waiter.pause(3);
        submitButton.click();
    }
}



