package testng_knowledge;

import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class _03_Ignoring_Test {


    @Test
    public void test1() {
        System.out.println("This is test 1");
    }


    @Ignore
    @Test
    public void test2() {
        System.out.println("This is test 2");
    }

    @Test(enabled = false)
    public void test3() {
        System.out.println("This is test 3");
    }
}