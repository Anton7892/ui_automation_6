package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

public class Project3Page {
    public Project3Page(){
        PageFactory.initElements(Driver.getDriver(),this);

    }

    @FindBy(css = "radio ml-0 ")
    public WebElement oneWayButton;

    @FindBy()
    public WebElement roundTripButton;
}
